﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyBlog.Models;
using System.Dynamic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace MyBlog.Controllers
{
    [Authorize(Roles = "admin")]
    public class ArticleController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Article
        [AllowAnonymous]
        public ActionResult Index()
        {
            //RoleManager<IdentityRole> spravceRoli = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new IdentityDbContext()));
            //spravceRoli.Create(new IdentityRole("admin"));
            //UserManager<ApplicationUser> spravceUzivatelu = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            //ApplicationUser uzivatel = spravceUzivatelu.FindByName("jiri.bardodej@seznam.cz");
            //spravceUzivatelu.AddToRole(uzivatel.Id, "admin");
            // klidně smaž
            

            return View(db.Articles.ToList());
        }
        [AllowAnonymous]
        // Filtruje články podle sekce
        public ActionResult FiltrSekce(string filtrsekce)
        {
            if(filtrsekce != null)
                {
                var filtrList = db.Articles
                                  .Where(r => r.Section == filtrsekce)
                                  .ToList();
                return View(filtrList);
                }
            return View();
        }

        // GET: Article/Details/5
        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Article article = db.Articles.Find(id);
            if (id == null)
            {
                return HttpNotFound();
            }
            return View(article);
        }

        // GET: Article/Create
        public ActionResult Create()
        {
            ViewBag.Sections = new SelectList(db.Sections, "Id", "Sekce");// Pro DropDownList
            return View();
        }

        // POST: Article/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,OpenText,Text,Section,Tags")] Article article)
        {
            if (ModelState.IsValid)
            {
                article.PublicationDate = DateTime.Now;//nastaví datum a čas publikace článku na PC datum a čas
                db.Articles.Add(article);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Sections = new SelectList(db.Sections, "Id", "Sekce");// Pro DropDownList
            return View(article);
        }

        // GET: Article/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.Sections = new SelectList(db.Sections, "Id", "Sekce");// Pro DropDownList
            Article article = db.Articles.Find(id);
            if (article == null)
            {
                return HttpNotFound();
            }
            return View(article);
        }

        // POST: Article/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,OpenText,Text,Section,Tags")] Article article)
        {
            if (ModelState.IsValid)
            {
                article.PublicationDate = DateTime.Now;//nastaví datum a čas publikace článku na PC datum a čas
                db.Entry(article).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(article);
        }

        // GET: Article/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Article article = db.Articles.Find(id);
            if (article == null)
            {
                return HttpNotFound();
            }
            return View(article);
        }

        // POST: Article/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Article article = db.Articles.Find(id);
            db.Articles.Remove(article);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
