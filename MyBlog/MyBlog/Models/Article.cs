﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyBlog.Models
{
      public class Article
    {
        public int Id { get; set; }
        [Display(Name = "Datum publikace")]
        public DateTime PublicationDate { get; set; }
        [Display(Name = "Nadpis")]
        [Required(ErrorMessage = "Vyplňte Nadpis")]
        [StringLength(50, ErrorMessage = "Nadpis je příliš dlouhý(max. 50 znaků)")]
        public string Title { get; set; }
        [Display(Name = "Úvodní text")]
        [Required(ErrorMessage = "Vyplňte úvodní text")]
        [StringLength(100, ErrorMessage = "Úvodní text je příliš dlouhý(max. 100 znaků)")]
        public string OpenText{ get; set; }
        [Display(Name = "Text blogu")]
        [AllowHtml]// kvůli vložení HTML do "Text"
        [Required(ErrorMessage = "Vyplňte text")]
        public string Text { get; set; }
        [Display(Name = "Sekce")]
        public string Section { get; set; }

    }
        
    
}