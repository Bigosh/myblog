﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyBlog.Models
{
    public class Section
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Název sekce musí být vyplněn")]
        public string Sekce { get; set; }
    }
}