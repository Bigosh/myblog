namespace MyBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateSectionClass : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Sections", "Sekce", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Sections", "Sekce", c => c.String());
        }
    }
}
